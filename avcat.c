#include <avcat.h>

int main(int argc, char *argv[])
{
  AVFormatContext *icontainer = NULL;
  AVFormatContext *ocontainer = NULL;
  int iter = 3;

  if(argc < 3)
    {
      fprintf(stderr, "[usage] avcat outputfile inputfile [..]\n");
      exit(EXIT_FAILURE);
    }

  av_register_all();

  /* 
   * Reading the first input file (argv[2]), this file
   * contains the proper header which contains stream, codec and
   * duration details. Following lines will setup the output
   * file (argv[1]) based on this input file. Also, ouput file header
   * will be written at this point.
   */

  icontainer = avcat_open_input(argv[2]);
  ocontainer = avcat_open_output(icontainer, argv[1]);
  avcat_encode_append(ocontainer, icontainer);
  avcat_close_input(icontainer);

  /* 
   * From the second input file (argv[3]), we need to
   * append the contents and should not write the header again.
   * Following loop will do it for us.
   */

  while(iter < argc)
    {
      icontainer = avcat_open_input(argv[iter]);
      avcat_encode_append(ocontainer, icontainer);
      avcat_close_input(icontainer);
      iter++;
    }

  avcat_close_output(ocontainer);
  return(0);
}
