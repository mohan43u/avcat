# Makefile for avcat

CC = gcc
OUTPUT = avcat

CFLAGS += $(shell pkg-config --cflags libavformat libavcodec)
CFLAGS += -I$(shell pwd) -O0 -ggdb
LIBS += $(shell pkg-config --libs libavformat libavcodec)

all: avcat

avcat: avcat.h avcat.c avcat_functions.c
	$(CC) $(CFLAGS) $(LIBS) -o $(OUTPUT) $^

clean:
	rm $(OUTPUT)

.PHONY: clean