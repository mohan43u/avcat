#ifndef __AVCAT__
#define __AVCAT__

#include <stdio.h>
#include <stdlib.h>
#include <libavformat/avformat.h>

#define BUFFERSIZE 1024

AVFormatContext* avcat_open_input(const char *inputfile);
void avcat_close_input(AVFormatContext *icontainer);
AVFormatContext* avcat_open_output(AVFormatContext *icontainer, const char *outputfilename);
void avcat_close_output(AVFormatContext *ocontainer);

#endif /*__AVCAT__*/
