#include <avcat.h>

static int herror(const char *func, int averror)
{
  if(averror < 0)
    {
      char errorstring[BUFFERSIZE];
      memset(errorstring, 0, BUFFERSIZE);
      av_strerror(averror, errorstring, BUFFERSIZE);
      fprintf(stderr, "%s: %s\n", func, errorstring);
      exit(EXIT_FAILURE);
    }
  return(averror);
}

static int herror2(const char *func, const char *errorstring)
{
  fprintf(stderr, "%s: %s\n", func, errorstring);
  exit(EXIT_FAILURE);
}

AVFormatContext* avcat_open_input(const char *inputfilename)
{
  AVFormatContext *icontainer = NULL;
  int iter = 0;

  /* 
   * Following lines will open and
   * setup input file for decoding.
   */
  herror("avcat_open_input()",
	 avformat_open_input(&icontainer, inputfilename, NULL, NULL));
  herror("avcat_open_input()",
	 avformat_find_stream_info(icontainer, NULL));

  /* 
   * Dumping inputfile details to
   * stdout/stderr.
   */
  av_dump_format(icontainer, 0, icontainer->filename, 0);

  return(icontainer);
}

void avcat_close_input(AVFormatContext *icontainer)
{
  avformat_close_input(&icontainer);
}

AVFormatContext* avcat_open_output(AVFormatContext *icontainer,
				      const char *outputfilename)
{
  AVFormatContext *ocontainer = NULL;
  AVStream *stream = NULL;
  AVStream *istream = NULL;
  AVCodec *codec = NULL;
  AVIOContext *pb = NULL;
  int iter = 0;

  ocontainer = avformat_alloc_context();
  if(ocontainer == NULL)
    herror2("avcat_open_output()", "avformat_alloc_context() returns NULL");
  strncpy(ocontainer->filename, outputfilename, sizeof(ocontainer->filename));

  ocontainer->oformat = av_guess_format(NULL, ocontainer->filename, NULL);
  if(ocontainer->oformat == NULL)
    herror2("avcat_open_output()", "av_guess_format() returns NULL");

  while(iter < icontainer->nb_streams)
    {
      istream = icontainer->streams[iter];
      if((codec = avcodec_find_encoder(istream->codec->codec_id)) == NULL)
	herror2("avcat_open_output()", "avcodec_find_encoder() returns NULL");

      if((stream = avformat_new_stream(ocontainer, codec)) == NULL)
	herror2("avcat_open_output()", "av_new_stream() returns NULL");

      /* 
       * Copying codec context from
       * first input file which
       * contains stream, codec and duration
       * details.
       */
      herror("avcat_open_output()",
	     avcodec_copy_context(stream->codec, istream->codec));

      /* 
       * I really dont know why libx264 dont like default settings
       * from ffmpeg/avconv. following lines will make libx264 happy.
       */
      stream->codec->qmin = 0;
      stream->codec->qmax = 0;
      stream->codec->qcompress = 0.0;
      stream->codec->b_quant_factor = 0.0;
      stream->sample_aspect_ratio.num = stream->codec->sample_aspect_ratio.num;
      stream->sample_aspect_ratio.den = stream->codec->sample_aspect_ratio.den;
      if(ocontainer->oformat->flags & AVFMT_GLOBALHEADER)
	ocontainer->oformat->flags ^= AVFMT_GLOBALHEADER;

      /* 
       * Opening the encoder codec. We should do this
       * before doing encoding.
       */
      herror("avcat_open_output()",
      	     avcodec_open2(stream->codec, codec, NULL));

      ocontainer->streams[iter] = stream;
      iter++;
    }

  /* 
   * Opening the output file for
   * writing and attaching it with output
   * container.
   */
  herror("avcat_open_output()", avio_open(&pb, ocontainer->filename, AVIO_FLAG_WRITE));
  ocontainer->pb = pb;

  /* 
   * Dumping outputfile container details
   * to stderr.
   */
  av_dump_format(ocontainer, 0, ocontainer->filename, 1);

  return(ocontainer);
}

void avcat_close_output(AVFormatContext *ocontainer)
{
  int iter = 0;

  while(iter < ocontainer->nb_streams)
    {
      avcodec_close(ocontainer->streams[iter]->codec);
      iter++;
    }
  avio_close(ocontainer->pb);
  avformat_free_context(ocontainer);
}

void avcat_encode_append(AVFormatContext *ocontainer, AVFormatContext *icontainer)
{
  AVPacket packet;
  AVDictionary *options = NULL;
  int returncode = 0;
  int iter = 0;

  /* 
   * This is how we prevent writing output header
   * again and again.
   */
  if(!ocontainer->priv_data)
    herror("avcat_encode_append()", avformat_write_header(ocontainer, NULL));

  /* 
   * Following loop will go through each stream and
   * append the input file content to output file
   */
  while(1)
    {
      returncode = av_read_frame(icontainer, &packet);
      if(returncode == AVERROR_EOF)
	break;

      /* 
       * Following if condition will prevent encoder exiting
       * when the second input file's starttime is already
       * covered by first input file, in other words, the second input file
       * overlaps with first input file.
       */
      if(ocontainer->streams[packet.stream_index]->cur_dts >= packet.dts)
	  ocontainer->streams[packet.stream_index]->cur_dts = AV_NOPTS_VALUE;

      herror("avcat_encode_append()", av_write_frame(ocontainer, &packet));
    }
}
